# NAME

Astro::FITS::CFITSIO::FileName - parse and generate CFITSIO extended file names.

# VERSION

version 0.09

# SYNOPSIS

    $fobj = Astro::FITS::CFITSIO::FileName->new( $filename );

    $attr = Astro::FITS::CFITSIO::FileName->parse_filename( $filename );

# DESCRIPTION

CFITSIO packs a lot of functionality into a filename; it can include
row and column filtering, binning and histogramming, image slicing,
etc.  It can be handy to manipulate the various parts that make up
a fully specified CFITSIO filename.

**Astro::FITS::CFITSIO::FileName** slices and dices a CFITSIO
extended filename into its constituent pieces.  Or, given the constituent
pieces, it can craft an extended CFITSIO filename.

Documentation for the CFITSIO extended filename syntax is available in Chapter 10 of

    https://heasarc.gsfc.nasa.gov/docs/software/fitsio/c/c_user/cfitsio.html

## Warning!

This module does not actually parse row filters, so it's possible that
it'll confuse an illegal component as one.  For example, in this
example, `foo.fits[1:512]`, the illegal, partial image specification
`[1:512]` will be identified as a row filter.  Oops.

## So many object attributes!

There are many attributes that an object might have; most are optional.
Each optional attribute has a corresponding predicate method
(`has_XXX`) which will return true if the attribute was set.  For
example, the `pix_filter` attribute is optional; to check if it was
set, use the `has_pix_filter` method.

## Manipulating a filename

`Astro::FITS::CFITSIO::FileName` objects are meant to be immutable, so
manipulating attributes has to be done outside of the object.

One way is to call the ["parse\_filename"](#parse_filename) class method on the original
filename. It returns a hash of attributes. Another is to call the
["to\_hash"](#to_hash) object method, which returns a hash of attributes for a
particular object.  Both of these hashes may be fed into the class
constructor.

# OBJECT ATTRIBUTES

## base\_filename

The name of the file, without any trailing specifications
(e.g. anything in square brackets or parenthesis) or a leading URI.

This attribute is always present.

## file\_type

The type of file.  CFITSIO uses a URI style specification (e.g. `mem://`, `http://` ).

This is optional.  See ["has\_file\_type"](#has_file_type)

## output\_name

An output or template name, depending upon the context that it's used with CFITSIO.

This is optional.  See ["has\_output\_name"](#has_output_name)

## extname

The name of the HDU provided by the `EXTNAME` header keyword.

This is optional. See ["has\_extname"](#has_extname)

## extver

The version of the HDU specified by the ["extname"](#extname) attribute.  ["extname"](#extname) must also be specified.

This is optional.  See ["has\_extver"](#has_extver).

## xtension

The type of the HDU.  It is case insensitive and may be one of

    A | ASCII |  I | IMAGE | T | TABLE | B | BINTABLE

This is optional.  See ["has\_xtension"](#has_xtension)

## image\_cell\_spec

Images can be stored in vector cells; this specifies which cell to access.

This is optional. See ["has\_image\_cell\_spec"](#has_image_cell_spec).

## hdunum

The HDU index. `0` is the primary HDU.  Do not use this with ["extname"](#extname).

This is optional. See ["has\_hdunum"](#has_hdunum).

## compress\_spec

The image tile compression specification. The specification is not validated.

This is optional. See ["has\_compress\_spec"](#has_compress_spec).

## image\_section

Returns an arrayref of two elements containing (as strings) the pixel ranges for the image axes.

This is optional. See ["has\_image\_section"](#has_image_section).

## pix\_filter

A hashref containing the pixel filter.

The hash has the following entries:

- datatype

    Optional.

- discard\_hdus

    Optional.

- expr

    The filter expression.

This is optional.  See ["has\_pix\_filter"](#has_pix_filter).

## col\_filter

Returns an array of column filters, as strings.

This is optional.  See ["has\_col\_filter"](#has_col_filter).

## row\_filter

Returns an array of row filters, as strings.

This is optional. See ["has\_row\_filter"](#has_row_filter)

## bin\_spec

Returns an arrayref of hashrefs containing binning/histogram specifications.

The hashes have the following entries:

- datatype

    Optional.

- expr

    The binning expression.

This is optional. See ["has\_bin\_spec"](#has_bin_spec).

## filename

This returns the full CFITSIO filename based on all of the attributes.

# CONSTRUCTORS

## new

    $fileobj = Astro::FITS::CFITSIO::FileName->new( $filename );
    $fileobj = Astro::FITS::CFITSIO::FileName->new( \%args );
    $fileobj = Astro::FITS::CFITSIO::FileName->new( \%args );

In the first example, parse a fully specified CFITSIO filename and populate
the attributes.

In the second example the following arguments are available.

- base\_filename _Str_
- filename _Str_

    One (and only one) of these is _Required_.

    If `base_filename` is specified, it provides the value for the `base_filename` attribute.

    If `filename` is provided, it is parsed using ["parse\_filename"](#parse_filename) and provides an initial
    set of attributes; additional attributes specified in the constructor will override them.

- file\_type _Enum_

    _Optional_

    One of the CFITSIO supported file types, as a string.

- output\_name _Str_

    _Optional_

- extname _Str_

    _Optional_

    Don't use this with the ["hdunum"](#hdunum) option.

- extver _PositiveInt_

    _Optional_

    Don't use this with ["hdunum"](#hdunum). ["extname"](#extname) must also be set.

- xtension _Enum_

    _Optional_

    The type of the HDU.  It is case insensitive and may be one of

        A | ASCII |  I | IMAGE | T | TABLE | B | BINTABLE

- image\_cell\_spec

    _Optional_

    Images can be stored in vector cells; this specifies which cell to access.
    Its value is not validated.

- hdunum _Positive Non Zero Integer_

    _Optional_

    The HDU index. `0` is the primary HDU.  Do not use this with ["extname"](#extname).

- compress\_spec

    _Optional_

    The image tile compression specification.
    Its value is not validated.

- image\_section

    _Optional_

    An array of two elements containing (as strings) the pixel ranges for the image axes.

- pix\_filter

    _Optional_

    A hashref containing the pixel filter.  The hash has the following entries:

    - datatype

        _Optional_

    - discard\_hdus

        _Optional_

    - expr

        _Required_ The filter expression.

- col\_filter

    _Optional_

    An array of column filters, as strings. Not validated.

- row\_filter

    _Optional_

    An array of row filters, as strings. Not validated.

- bin\_spec

    _Optional_

    An arrayref of hashrefs containing binning/histogram specifications.

    The hashes have the following entries:

    - datatype

        Optional.

    - expr

        The binning expression.

## clone\_with

    $new = $old->clone_with( %attr | \%attr );

Create a new `Astro::FITS::CFITSIO::FileName` object based on an
existing one with the specified modified attributes.

# CLASS METHODS

## parse\_filename

    $attr = $class->parse_filename( $filename );

Parses an extended CFITSIO filename and returns a hash which may be
fed into the `Astro::FITS::CFITSIO::FileName` constructor.  Typically
it's easier to just call the class constructor with the filename as a
single argument.

If a subclass needs to amend the parsing, this is the method to
override.

# METHODS

## has\_file\_type

returns true if the ["file\_type"](#file_type) attribute is present.

## has\_output\_name

returns true if the ["output\_name"](#output_name) attribute is present.

## has\_extname

returns true if the ["extname"](#extname) attribute is present.

## has\_extver

returns true if the ["extver"](#extver) attribute is present.

## has\_xtension

returns true if the ["xtension"](#xtension) attribute is present.

## has\_image\_cell\_spec

returns true if the ["image\_cell\_spec"](#image_cell_spec) attribute is present.

## has\_hdunum

returns true if the ["hdunum"](#hdunum) attribute is present.

## has\_compress\_spec

returns true if the ["compress\_spec"](#compress_spec) attribute is present.

## has\_image\_section

returns true if the ["image\_section"](#image_section) attribute is present.

## has\_pix\_filter

returns true if the ["pix\_filter"](#pix_filter) attribute is present.

## has\_col\_filter

returns true if the ["col\_filter"](#col_filter) attribute is present.

## has\_row\_filter

returns true if the ["row\_filter"](#row_filter) attribute is present.

## has\_bin\_spec

returns true if the ["bin\_spec"](#bin_spec) attribute is present.

## render\_base\_filename

## render\_file\_type

## render\_output\_name

## render\_compress\_spec

## render\_hdu

## render\_image\_section

## render\_pix\_filter

## render\_col\_filter

## render\_row\_filter

## render\_bin\_spec

return a string version of the attribute which can be concatenated
to construct an extended syntax CFITSIO filename.  For example,

    $self->render_hdu

might return

    [extname, 2]

## is\_compressed

    $bool = $file->is_compressed.

Returns true if the filename ends in `.gz`.

## to\_hash (deprecated)

## TO\_HASH

    $hash = $self->TO_HASH;

returns a hashref containing the object's attributes.  This can be used to modify the attributes
and then pass them to the class constructor to create a modified object.

# OVERLOAD

## ""

Stringification is overloaded to return the filename as returned by the ["filename"](#filename) attribute.

# INTERNALS

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-astro-fits-cfitsio-filename@rt.cpan.org  or through the web interface at: [https://rt.cpan.org/Public/Dist/Display.html?Name=Astro-FITS-CFITSIO-FileName](https://rt.cpan.org/Public/Dist/Display.html?Name=Astro-FITS-CFITSIO-FileName)

## Source

Source is available at

    https://gitlab.com/djerius/astro-fits-cfitsio-filename

and may be cloned from

    https://gitlab.com/djerius/astro-fits-cfitsio-filename.git

# SEE ALSO

Please see those modules/websites for more information related to this module.

- [Astro::FITS::CFITSIO](https://metacpan.org/pod/Astro%3A%3AFITS%3A%3ACFITSIO)
- [Astro::FITS::CFITSIO::Simple](https://metacpan.org/pod/Astro%3A%3AFITS%3A%3ACFITSIO%3A%3ASimple)
- [Astro::FITS::CFITSIO::Utils](https://metacpan.org/pod/Astro%3A%3AFITS%3A%3ACFITSIO%3A%3AUtils)

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2021 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
