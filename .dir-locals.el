(
 (cperl-mode . (
           (flycheck-perl-executable . "plx-perl" )
           (flycheck-perl-perlcritic-executable . "plx-perlcritic" )
           (perltidy-program . "plx-perltidy" )
             )
 )
)
