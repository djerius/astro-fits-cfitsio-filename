package Astro::FITS::CFITSIO::FileName::Types;

# ABSTRACT: Types for Astro::FITS::CFITSIO::FileName

use v5.26;
use warnings;

our $VERSION = '0.09';

use Type::Library -base;
use Types::Standard 'Str', 'HashRef';
use Type::Tiny::Class;

require Astro::FITS::CFITSIO::FileName;    # prevent import loop, just in case.

=type FitsFileName

This is a class type for L<Astro::FITS::CFITSIO::FileName>.  It has
coercions from L<Types::Standard/Str> and L<Types::Standard/HashRef> to
a L<Astro::FITS::CFITSIO::FileName> object.

=cut

__PACKAGE__->add_type(
    Type::Tiny::Class->new(
        name  => 'FitsFileName',
        class => 'Astro::FITS::CFITSIO::FileName',
    )->plus_constructors( Str, 'new', HashRef, 'new', ),
);

# COPYRIGHT

1;

__END__

=for stopwords
FitsFileName

=head1 DESCRIPTION

This is a L<Type::Tiny> library.

=cut
